EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micro-cache
LIBS:XPRESSO1769
LIBS:nxp_armmcu
LIBS:lm2596
LIBS:relay
LIBS:Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date "22 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4350 1750 0    60   Input ~ 0
EXT_POWX
NoConn ~ 4350 1850
NoConn ~ 4350 1950
Text HLabel 4350 2850 0    60   Output ~ 0
TXD1
Text HLabel 4350 2950 0    60   Input ~ 0
RXD1
NoConn ~ 4350 2050
NoConn ~ 4350 2150
NoConn ~ 4350 2250
NoConn ~ 4350 2350
NoConn ~ 4350 2450
NoConn ~ 4350 2550
NoConn ~ 4350 2650
NoConn ~ 4350 2750
NoConn ~ 4350 3050
NoConn ~ 4350 3150
NoConn ~ 4350 3250
NoConn ~ 4350 3350
NoConn ~ 4350 3450
NoConn ~ 4350 3550
NoConn ~ 4350 3650
NoConn ~ 4350 3750
NoConn ~ 4350 3850
NoConn ~ 4350 3950
NoConn ~ 4350 4050
NoConn ~ 4350 4150
NoConn ~ 4700 4850
NoConn ~ 4800 4850
NoConn ~ 4900 4850
NoConn ~ 5000 4850
NoConn ~ 5100 4850
NoConn ~ 5200 4850
NoConn ~ 5300 4850
NoConn ~ 5400 4850
NoConn ~ 5500 4850
NoConn ~ 5600 4850
NoConn ~ 5700 4850
NoConn ~ 5800 4850
NoConn ~ 5900 4850
NoConn ~ 6000 4850
NoConn ~ 6100 4850
NoConn ~ 6200 4850
NoConn ~ 6300 4850
NoConn ~ 6400 4850
NoConn ~ 6650 2350
NoConn ~ 6650 2250
NoConn ~ 6650 2150
NoConn ~ 6650 2050
NoConn ~ 6650 1650
$Comp
L LPCxpresso1769 U4
U 1 1 55AD4539
P 5000 2100
F 0 "U4" H 6350 2700 60  0000 C CNN
F 1 "LPCxpresso1769" H 4950 2700 60  0000 C CNN
F 2 "" H 5000 2100 60  0001 C CNN
F 3 "" H 5000 2100 60  0001 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
Text HLabel 4350 1650 0    60   Input ~ 0
GNDX
Text HLabel 6650 3050 2    60   BiDi ~ 0
P2[0]
Text HLabel 6650 3150 2    60   BiDi ~ 0
P2[1]
Text HLabel 6650 3250 2    60   BiDi ~ 0
P2[2]
Text HLabel 6650 3350 2    60   BiDi ~ 0
P2[3]
Text HLabel 6650 3450 2    60   BiDi ~ 0
P2[4]
Text HLabel 6650 3550 2    60   BiDi ~ 0
P2[5]
Text HLabel 6650 3650 2    60   BiDi ~ 0
P2[6]
Text HLabel 6650 3750 2    60   BiDi ~ 0
P2[7]
Text HLabel 6650 3850 2    60   BiDi ~ 0
P2[8]
Text HLabel 4600 4850 3    60   BiDi ~ 0
P2[9]
Text HLabel 6650 3950 2    60   BiDi ~ 0
P2[10]
Text HLabel 6650 4050 2    60   BiDi ~ 0
P2[11]
Text HLabel 6650 4150 2    60   BiDi ~ 0
P2[12]
Text HLabel 6650 4250 2    60   Output ~ 0
GNDX
Text HLabel 4350 4250 0    60   BiDi ~ 0
P2[13]
Text HLabel 6650 2950 2    60   BiDi ~ 0
P0[11]
Text HLabel 6650 2850 2    60   BiDi ~ 0
P0[10]
NoConn ~ 6650 2750
NoConn ~ 6650 2650
NoConn ~ 6650 2550
NoConn ~ 6650 2450
$EndSCHEMATC
