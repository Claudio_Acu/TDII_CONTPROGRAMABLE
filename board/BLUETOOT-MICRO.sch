EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micro-cache
LIBS:XPRESSO1769
LIBS:nxp_armmcu
LIBS:lm2596
LIBS:relay
LIBS:Board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date "22 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RN-41 U1
U 1 1 55AC6ABF
P 5800 3500
F 0 "U1" H 6400 2550 50  0000 C CNN
F 1 "RN-42" H 5800 4450 50  0000 C CNN
F 2 "" H 5800 3500 60  0001 C CNN
F 3 "" H 5800 3500 60  0001 C CNN
	1    5800 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 55AC6AC0
P 5150 2950
F 0 "#PWR01" H 5150 2700 50  0001 C CNN
F 1 "GND" H 5150 2800 50  0000 C CNN
F 2 "" H 5150 2950 60  0001 C CNN
F 3 "" H 5150 2950 60  0001 C CNN
	1    5150 2950
	0    1    1    0   
$EndComp
$Comp
L GND #PWR02
U 1 1 55AC6AC1
P 5150 4050
F 0 "#PWR02" H 5150 3800 50  0001 C CNN
F 1 "GND" H 5150 3900 50  0000 C CNN
F 2 "" H 5150 4050 60  0001 C CNN
F 3 "" H 5150 4050 60  0001 C CNN
	1    5150 4050
	0    1    1    0   
$EndComp
$Comp
L GND #PWR03
U 1 1 55AC6AC2
P 5550 4550
F 0 "#PWR03" H 5550 4300 50  0001 C CNN
F 1 "GND" H 5550 4400 50  0000 C CNN
F 2 "" H 5550 4550 60  0001 C CNN
F 3 "" H 5550 4550 60  0001 C CNN
	1    5550 4550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 55AC6AC3
P 6050 4550
F 0 "#PWR04" H 6050 4300 50  0001 C CNN
F 1 "GND" H 6050 4400 50  0000 C CNN
F 2 "" H 6050 4550 60  0001 C CNN
F 3 "" H 6050 4550 60  0001 C CNN
	1    6050 4550
	1    0    0    -1  
$EndComp
$Comp
L Led_Small D1
U 1 1 55AC6AC5
P 7100 3250
F 0 "D1" H 7050 3350 50  0000 L CNN
F 1 "C_STATUS" H 6950 3170 50  0000 L CNN
F 2 "" H 7100 3250 60  0001 C CNN
F 3 "" H 7100 3250 60  0001 C CNN
	1    7100 3250
	-1   0    0    1   
$EndComp
$Comp
L Led_Small D2
U 1 1 55AC6AC6
P 7100 3550
F 0 "D2" H 7050 3650 50  0000 L CNN
F 1 "STATUS" H 6950 3470 50  0000 L CNN
F 2 "" H 7100 3550 60  0001 C CNN
F 3 "" H 7100 3550 60  0001 C CNN
	1    7100 3550
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 55AC6AC8
P 6750 3250
F 0 "R1" V 6830 3250 50  0000 C CNN
F 1 "220" V 6750 3250 50  0000 C CNN
F 2 "" H 6750 3250 60  0001 C CNN
F 3 "" H 6750 3250 60  0001 C CNN
	1    6750 3250
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 55AC6AC9
P 7550 3550
F 0 "R2" V 7630 3550 50  0000 C CNN
F 1 "220" V 7550 3550 50  0000 C CNN
F 2 "" H 7550 3550 60  0001 C CNN
F 3 "" H 7550 3550 60  0001 C CNN
	1    7550 3550
	0    1    1    0   
$EndComp
$Comp
L GND #PWR05
U 1 1 55AC6ACA
P 7750 3600
F 0 "#PWR05" H 7750 3350 50  0001 C CNN
F 1 "GND" H 7750 3450 50  0000 C CNN
F 2 "" H 7750 3600 60  0001 C CNN
F 3 "" H 7750 3600 60  0001 C CNN
	1    7750 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3950 5200 3950
Wire Wire Line
	5150 4050 5200 4050
Wire Wire Line
	5150 2950 5200 2950
Wire Wire Line
	6050 4550 6050 4500
Wire Wire Line
	5550 4550 5550 4500
Wire Wire Line
	7150 3850 6800 3850
Wire Wire Line
	6800 3850 6800 3950
Wire Wire Line
	6800 3950 6400 3950
Wire Wire Line
	6800 4100 7150 4100
Wire Wire Line
	6800 4100 6800 4050
Wire Wire Line
	6800 4050 6400 4050
Wire Wire Line
	7000 3250 6900 3250
Wire Wire Line
	6600 3250 6400 3250
Wire Wire Line
	7200 3250 7450 3250
Wire Wire Line
	7400 3550 7200 3550
Wire Wire Line
	7000 3550 6550 3550
Wire Wire Line
	6550 3550 6550 3450
Wire Wire Line
	6550 3450 6400 3450
Wire Wire Line
	7700 3550 7750 3550
Wire Wire Line
	7750 3550 7750 3600
Text Notes 3700 5350 0    60   ~ 0
C_STATUS: El estado se pone en alto si esta conectado a un dispositivo.\nSTATUS: Muestra en que modo esta el bluetoot, se apaga cuando se vincula a otro dispositivo
Text HLabel 7450 3250 2    60   Input ~ 0
3.3V
Text HLabel 4900 3950 0    60   Input ~ 0
3.3V
Text HLabel 7150 3850 2    60   Input ~ 0
TX
Text HLabel 7150 4100 2    60   Input ~ 0
RX
NoConn ~ 5200 3050
NoConn ~ 5200 3150
NoConn ~ 5200 3250
NoConn ~ 5200 3350
NoConn ~ 5200 3450
NoConn ~ 5200 3550
NoConn ~ 5200 3650
NoConn ~ 5200 3750
NoConn ~ 5200 3850
NoConn ~ 5450 4500
NoConn ~ 5650 4500
NoConn ~ 5750 4500
NoConn ~ 5850 4500
NoConn ~ 5950 4500
NoConn ~ 6150 4500
NoConn ~ 6400 3850
NoConn ~ 6400 3750
NoConn ~ 6400 3650
NoConn ~ 6400 3550
NoConn ~ 6400 3350
NoConn ~ 6400 3150
NoConn ~ 6400 3050
NoConn ~ 6400 2950
$EndSCHEMATC
