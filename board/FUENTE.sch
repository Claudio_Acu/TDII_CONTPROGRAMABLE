EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micro-cache
LIBS:XPRESSO1769
LIBS:nxp_armmcu
LIBS:lm2596
LIBS:relay
LIBS:Board-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 3 6
Title ""
Date "22 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LD1117S33CTR U3
U 1 1 55AC7D3B
P 7850 5400
F 0 "U3" H 7850 5650 40  0000 C CNN
F 1 "LD1117S33CTR" H 7850 5600 40  0000 C CNN
F 2 "SMD_Packages:SOT-223" H 7850 5500 40  0001 C CNN
F 3 "" H 7850 5400 60  0001 C CNN
	1    7850 5400
	1    0    0    -1  
$EndComp
$Comp
L LM2575-5 U2
U 1 1 55AC7D3C
P 5150 5400
F 0 "U2" H 4800 5800 60  0000 C CNN
F 1 "LM2575-5" H 5250 5800 60  0000 C CNN
F 2 "SMD_Packages:TO-263-5" H 5450 5350 60  0001 C CNN
F 3 "" H 5150 5400 60  0001 C CNN
	1    5150 5400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 55AC7D3D
P 3350 5200
F 0 "P1" H 3350 5350 50  0000 C CNN
F 1 "12V" V 3450 5200 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02" H 3350 5200 60  0001 C CNN
F 3 "" H 3350 5200 60  0001 C CNN
	1    3350 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3550 5150 4550 5150
$Comp
L GND #PWR06
U 1 1 55AC7D3E
P 3600 5400
F 0 "#PWR06" H 3600 5150 60  0001 C CNN
F 1 "GND" H 3600 5250 60  0000 C CNN
F 2 "" H 3600 5400 60  0001 C CNN
F 3 "" H 3600 5400 60  0001 C CNN
	1    3600 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 5250 3600 5250
Wire Wire Line
	3600 5250 3600 5400
$Comp
L GND #PWR07
U 1 1 55AC7D3F
P 4850 6000
F 0 "#PWR07" H 4850 5750 60  0001 C CNN
F 1 "GND" H 4850 5850 60  0000 C CNN
F 2 "" H 4850 6000 60  0001 C CNN
F 3 "" H 4850 6000 60  0001 C CNN
	1    4850 6000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 55AC7D40
P 5050 6000
F 0 "#PWR08" H 5050 5750 60  0001 C CNN
F 1 "GND" H 5050 5850 60  0000 C CNN
F 2 "" H 5050 6000 60  0001 C CNN
F 3 "" H 5050 6000 60  0001 C CNN
	1    5050 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6000 4850 5950
Wire Wire Line
	5050 6000 5050 5950
$Comp
L CAPAPOL C1
U 1 1 55AC7D41
P 4050 5450
F 0 "C1" H 4100 5550 50  0000 L CNN
F 1 "100u" H 4100 5350 50  0000 L CNN
F 2 "Discret:C1.5V8V" H 4150 5300 30  0001 C CNN
F 3 "" H 4050 5450 60  0001 C CNN
	1    4050 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5250 4050 5150
Connection ~ 4050 5150
$Comp
L GND #PWR09
U 1 1 55AC7D42
P 4050 5700
F 0 "#PWR09" H 4050 5450 60  0001 C CNN
F 1 "GND" H 4050 5550 60  0000 C CNN
F 2 "" H 4050 5700 60  0001 C CNN
F 3 "" H 4050 5700 60  0001 C CNN
	1    4050 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5700 4050 5650
$Comp
L DIODESCH D3
U 1 1 55AC7D43
P 5850 5650
F 0 "D3" H 5850 5750 50  0000 C CNN
F 1 "1N5819" H 5850 5550 50  0000 C CNN
F 2 "Discret:DO-41" H 5850 5650 60  0001 C CNN
F 3 "" H 5850 5650 60  0001 C CNN
	1    5850 5650
	0    -1   -1   0   
$EndComp
$Comp
L INDUCTOR_SMALL L1
U 1 1 55AC7D44
P 6350 5350
F 0 "L1" H 6350 5450 50  0000 C CNN
F 1 "330u" H 6350 5300 50  0000 C CNN
F 2 "Discret:C1.5V8V" H 6350 5350 60  0001 C CNN
F 3 "" H 6350 5350 60  0001 C CNN
	1    6350 5350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 55AC7D45
P 5850 5900
F 0 "#PWR010" H 5850 5650 60  0001 C CNN
F 1 "GND" H 5850 5750 60  0000 C CNN
F 2 "" H 5850 5900 60  0001 C CNN
F 3 "" H 5850 5900 60  0001 C CNN
	1    5850 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 5900 5850 5850
Wire Wire Line
	5850 5450 5850 5350
Connection ~ 5850 5350
Wire Wire Line
	6600 5350 7450 5350
Wire Wire Line
	6800 4850 6800 5450
Wire Wire Line
	5700 5150 6950 5150
Connection ~ 6800 5350
$Comp
L CAPAPOL C2
U 1 1 55AC7D46
P 6800 5650
F 0 "C2" H 6850 5750 50  0000 L CNN
F 1 "330u" H 6850 5550 50  0000 L CNN
F 2 "Discret:C1.5V8V" H 6900 5500 30  0001 C CNN
F 3 "" H 6800 5650 60  0001 C CNN
	1    6800 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 55AC7D47
P 6800 5900
F 0 "#PWR011" H 6800 5650 60  0001 C CNN
F 1 "GND" H 6800 5750 60  0000 C CNN
F 2 "" H 6800 5900 60  0001 C CNN
F 3 "" H 6800 5900 60  0001 C CNN
	1    6800 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5900 6800 5850
$Comp
L GND #PWR012
U 1 1 55AC7D48
P 7850 5750
F 0 "#PWR012" H 7850 5500 60  0001 C CNN
F 1 "GND" H 7850 5600 60  0000 C CNN
F 2 "" H 7850 5750 60  0001 C CNN
F 3 "" H 7850 5750 60  0001 C CNN
	1    7850 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5750 7850 5650
$Comp
L CAPAPOL C3
U 1 1 55AC7D49
P 8500 5650
F 0 "C3" H 8550 5750 50  0000 L CNN
F 1 "10u" H 8550 5550 50  0000 L CNN
F 2 "Discret:C1.5V8V" H 8600 5500 30  0001 C CNN
F 3 "" H 8500 5650 60  0001 C CNN
	1    8500 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 55AC7D4A
P 8500 5900
F 0 "#PWR013" H 8500 5650 60  0001 C CNN
F 1 "GND" H 8500 5750 60  0000 C CNN
F 2 "" H 8500 5900 60  0001 C CNN
F 3 "" H 8500 5900 60  0001 C CNN
	1    8500 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5900 8500 5850
Wire Wire Line
	8500 5200 8500 5450
Connection ~ 6800 5150
$Comp
L PWR_FLAG #FLG014
U 1 1 55AC7D54
P 6950 5150
F 0 "#FLG014" H 6950 5245 30  0001 C CNN
F 1 "PWR_FLAG" H 6950 5330 30  0000 C CNN
F 2 "" H 6950 5150 60  0001 C CNN
F 3 "" H 6950 5150 60  0001 C CNN
	1    6950 5150
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG015
U 1 1 55AC7D55
P 3600 5100
F 0 "#FLG015" H 3600 5195 30  0001 C CNN
F 1 "PWR_FLAG" H 3600 5280 30  0000 C CNN
F 2 "" H 3600 5100 60  0001 C CNN
F 3 "" H 3600 5100 60  0001 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5100 3600 5150
Connection ~ 3600 5150
Connection ~ 3600 5250
Wire Wire Line
	5700 5350 6100 5350
Wire Wire Line
	8250 5350 8500 5350
Connection ~ 8500 5350
Text HLabel 8500 5200 1    60   Input ~ 0
3.3V
Text HLabel 6800 4850 1    60   Output ~ 0
5V
$Comp
L GND #PWR016
U 1 1 55AD0409
P 10650 5550
F 0 "#PWR016" H 10650 5300 50  0001 C CNN
F 1 "GND" H 10650 5400 50  0000 C CNN
F 2 "" H 10650 5550 60  0001 C CNN
F 3 "" H 10650 5550 60  0001 C CNN
	1    10650 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 5450 10650 5550
Text HLabel 10650 5450 1    60   Input ~ 0
GND
$EndSCHEMATC
